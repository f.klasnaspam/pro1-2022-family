package cz.uhk.pro1.family.util;

public interface List {

    public void add(Object element);
    public void add(Object element, int index);
    public void remove(int index);
    public Object get(int index);
    public int size();

}
