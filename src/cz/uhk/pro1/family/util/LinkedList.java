package cz.uhk.pro1.family.util;

public class LinkedList implements List {

    private Node first;
    private int size; // aby se velikost nemusela vždy dopočítávat, tj. procházet celý seznam

    @Override
    public void add(Object element) {
        if (first == null) {
            first = new Node(element, null);
        } else {
            Node node = first;
            while (node.next != null) {
                node = node.next;
            }
            node.next = new Node(element, null);
        }
        size++;
    }

    @Override
    public void add(Object element, int index) {

    }

    @Override
    public void remove(int index) {

    }

    @Override
    public Object get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        Node node = first;
        for (int i = 0; i < index; i++){
            node = node.next;
        }
        return node.item;
    }

    @Override
    public int size() {
        return size;
    }

    private class Node {

        private Object item;
        private Node next;

        private Node(Object item, Node next) {
            this.item = item;
            this.next = next;
        }

    }

}
