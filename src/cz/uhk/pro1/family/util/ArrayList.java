package cz.uhk.pro1.family.util;

public class ArrayList implements List {

    private Object[] elements = new Object[0];

    @Override
    public void add(Object element) {
        Object[] newElements = new Object[elements.length + 1];
        copyArrayElements(elements, 0, newElements, 0, elements.length);
        newElements[newElements.length - 1] = element;
        elements = newElements;
        // výkonově by nemuselo být ideální
    }

    @Override
    public void add(Object element, int index) {

    }

    @Override
    public void remove(int index) {

    }

    @Override
    public Object get(int index) {
        if (index < 0 || index >= elements.length) {
            throw new IndexOutOfBoundsException();
        }
        return elements[index];
    }

    @Override
    public int size() {
        return elements.length;
    }

    private void copyArrayElements(Object[] src, int srcPos, Object[] dest, int destPos, int length) {
        for (int i = 0; i < length; i++) {
            dest[destPos + i] = src[srcPos + i];
        }
    } // System.arraycopy(src, srcPos, dest, destPos, length);

}
